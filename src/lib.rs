use rand::Rng;
// use std::error::Error;

pub struct Config {
    pub length: i32,
}

impl Config {
    pub fn new(args: &[String]) -> Result<Config, &'static str> {
        if args.len() < 1 {
            return Err("not enough arguments");
        }
        let length: i32 = match args[1].parse() {
            Ok(n) => n,
            Err(_) => {
                return Err("error: First argument is not a number");
            }
        };
        Ok(Config { length })
    }
}

pub fn generate_password(config: Config) -> String {
    const CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz0123456789()-!@_#$%^+=ABCDEFGHIJKLMNOPQRST$";
    let pass_len = config.length;
    let mut rng = rand::thread_rng();
    let password: String = (0..pass_len)
        .map(|_| {
            let index = rng.gen_range(0, CHARSET.len());
            CHARSET[index] as char
        })
        .collect();
    return password;
}

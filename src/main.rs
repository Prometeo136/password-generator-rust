use password_generator::Config;
use rand::Rng;
use std::env;
use std::process;

fn main() {
    let args: Vec<String> = env::args().collect();
    let config = Config::new(&args).unwrap_or_else(|err| {
        eprintln!("Problem parsing arguments: {}", err);
        process::exit(1);
    });
    let generated_password = password_generator::generate_password(config);

    println!("{}", generated_password);
}
